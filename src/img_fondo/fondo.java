
package img_fondo;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.*;

/**
 *
 * @author gera
 */
public class fondo extends JDesktopPane{
 
       private  Image IMG=new ImageIcon(getClass().getResource("/imagenes/CONAFOR_150.png")).getImage();
 
       @Override
        public void paint(Graphics g){
            g.drawImage(IMG, 0, 0, getWidth(), getHeight(), this);
            super.paint(g);
        }
    }
 
